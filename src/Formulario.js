import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';


class FormularioSitios extends React.Component{
    enviarForm(valores,acciones){
        console.log(valores);

        
        fetch(
            'http://localhost:4000/insertar',
             {
                 method:'POST',
             
                 headers:{'Content-type':'application/json',
                        'cors':'Access-Control-Allow-Origin'},
                body:JSON.stringify({
                    user:{

                        nombre: valores.nombre,
                        nombre2:valores.nombre2,
                        direccion:valores.direccion,
                        barrio: valores.barrio,
                        descripcion: valores.descripcion,
                        latitud: valores.latitud,
                        longitud: valores.longitud,
                        
                    }
                    
                }),
                
             }     
        );
    };
    render(){
        let elemento=<Formik
            initialValues={
                {
                    nombre:'',
                    nombre2:'',
                    direccion:'',
                    barrio:'',
                    descripcion:'',
                    latitud:'',
                    longitud:''
                }

            }
            onSubmit={this.enviarForm}

            validationSchema={Yup.object().shape(
                {
                    nombre:Yup.string().required('Campo es Obligatorio'),           
                    direccion:Yup.string().required('Campo es Obligatorio'),
                    barrio:Yup.string().required('Campo es obligatorio'),
                    descripcion:Yup.string().required('Campo es Obligatorio'),
                    latitud:Yup.number().typeError('Debe ser un numero').required('Es obligatorio'),
                    longitud:Yup.number().typeError('Debe ser un numero').required('Es obligatorio'),
                }
            )}
            >
                <Container className="p-3">
                    
                    <Form>
                    
                        <div className="jumbotron">
                            <h1 className="text-center">Formulario-Sitios de interes</h1>
                            <p className="lead text-center"> Completar el siguiente formulario con la informacion del sitio de interes que
                                deseas agregar
                            </p>
                            <div className="row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="nombre"><b>Nombre</b></label>
                                    <Field name="nombre" type="text" className="form-control" placeholder="Ej:Restaurante El buen sabor"/> 
                                    <ErrorMessage name="nombre" className="invalid-feedback"></ErrorMessage>
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="nombre2"><b>Nombre 2</b></label>
                                    <Field name="nombre2" type="text" className="form-control" placeholder="(opcional)"/> 
                                </div>
                            </div> 
                            <div className="form-group col-md-13">
                                <label htmlFor="direccion"><b>Direccion</b></label>
                                <Field name="direccion" type="text" className="form-control" placeholder="Ej: Calle 13 sur #25-14"/>
                                <ErrorMessage name="direccion" className="invalid-feedback"></ErrorMessage>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-5">
                                    <label htmlFor="barrio"><b>Barrio</b></label>
                                    <Field name="barrio" type="text" className="form-control" placeholder="Ej: Banderas" rows="3"/>
                                    <ErrorMessage name="barrio" className="invalid-feedback"></ErrorMessage>
                                </div>
                                <div className="form-group col-md-6 ">
                                    <label htmlFor="descripcion" ><b>Descripcion</b></label>
                                    <Field name="descripcion" type="text" className="form-control" placeholder="Maximo 40 palabras"/>
                                    <ErrorMessage name="descripcion" className="invalid-feedback"></ErrorMessage>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-5">
                                    <label htmlFor="latitud"><b>Latitud</b></label>
                                    <Field name="latitud" type="text" className="form-control" placeholder="Valor de la latitud, puede ser consultada en maps" rows="3"/>
                                    <ErrorMessage name="latitud" className="invalid-feedback"></ErrorMessage>
                                </div>
                                <div className="form-group col-md-6 ">
                                    <label htmlFor="longitud" ><b>Longitud</b></label>
                                    <Field name="longitud" type="text" className="form-control" placeholder="Valor de la longitud, puede ser consultada en maps"/>
                                    <ErrorMessage name="longitud" className="invalid-feedback"></ErrorMessage>
                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label htmlFor="imagen"><b>Insertar imagen</b></label>
                                <Field name="imagen" type="file" className="form-control-file"/>
                            </div>
                            <div className="form-group text-center">
                                <Button type="submit" className="btn btn-lg btn-primary m-4">Enviar</Button>
                                <Button type="reset" className="btn btn-lg btn-secondary">Cancelar</Button>
                            </div>
                            
                        </div>
                        
                    </Form> 
                    
                </Container>
            </Formik>;
        return elemento;
    };
}

export default FormularioSitios;