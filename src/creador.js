import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';
import Sitios from './Sitios';
import Inicio from './Inicio';
import VidaLocalidad from './VidaLocalidad';
import FormularioEventos from './FormularioEve';
import FormularioSitios from './Formulario';


class Creador extends React.Component{
    render(){
        return (
        <div className="container">
            <Router>
                <div className="text-center p-3">  
                    <Link to="/Sitios-de-interes" className="btn btn-outline-danger">SITIOS DE INTERÉS</Link>
                    <Link to="/FormularioSitios" className="btn btn-outline-danger">FORMULARIO SITIOS</Link>
                    <Link to="/Eventos" className="btn btn-outline-warning">VIDA EN LA LOCALIDAD</Link>
                    <Link to="/FormularioEventos" className="btn btn-outline-warning">FORMULARIO EVENTOS</Link>
                    <Link to="/" className="btn btn-outline-success">VOLVER AL INICIO</Link>
                </div>    
            
                    <Switch>
                        <Route path='/' exact>
                            <Inicio/>
                        </Route>
                        <Route path='/Sitios-de-interes'>
                            <Sitios/>
                        </Route>
                        <Route path="/FormularioSitios">
                            <FormularioSitios/>
                        </Route>
                        <Route path="/Eventos">
                            <VidaLocalidad/>
                        </Route>
                        <Route path="/FormularioEventos">
                            <FormularioEventos/>
                        </Route>
                    </Switch>
                
            </Router>
            
        </div>
        );
    };



}

export default Creador;
