import React from 'react';
import "./estilo.css";
import {Animated} from "react-animated-css";

class Inicio extends React.Component{
    render(){
        return(
            <div className="app container">
      
      <img src="https://secretariageneral.gov.co/sites/default/files/logo-bogota.png" className="img-ts img-thumbnail rounded float-right" alt=""></img>

        <div className="jumbotron" >
        <div className="text-center">
        <Animated animationIn="bounceInRight" animationOut="fadeOut" isVisible={true}>
        <h1 className="text-danger" >KENNEDY</h1>
        </Animated>
        <img src="https://img.huffingtonpost.com/asset/5c8b7d78360000e01c6d3775.jpeg?ops=1200_630" className="img-tsa img-fluid" alt=""></img>
        </div>
       <div className="row">
           <div className="col-md-6">
           <Animated animationIn="tada" animationOut="fadeOut" isVisible={true}>
        <p className="text-center">
                         Creamos esta pagina web
                            para que conocieras más de esta
                         característica localidad de la ciudad
                                    de Bogota D.C
        </p>
        </Animated>
        </div>
        <div className="cold-md-6">
        <img src="https://bogota.gov.co/sites/default/files/2019-12/monumento_las_banderas_0.jpg" className="img-tsb img-fluid" alt=""></img>
        </div>
        </div>
        <img src="https://img.huffingtonpost.com/asset/5c8b7d78360000e01c6d3775.jpeg?ops=1200_630" className="img-tsa img-fluid" alt=""></img>
        
      </div>
          </div>
        );
    }
}

export default Inicio;
