import React from 'react';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';


class FormularioEventos extends React.Component{
    enviarForm(valores,acciones){
        console.log(valores);

        let datos={
            nombre:valores.nombre,
            direccion:valores.direccion,
            fechayhora:valores.fechayhora,
            latitud:valores.latitud,
            longitud:valores.longitud,
            barrio:valores.barrio,
            descripcion:valores.descripcion

        }
        fetch(
            'http://localhost:4000/ingresar',
             {
                 method:'POST',
                 
                 headers:{'Content-type':'application/json',
                 'cors':'Access-Control-Allow-Origin' },
                body:JSON.stringify( {

                
                    user:{
                        nombre:valores.nombre,
                        direccion:valores.direccion,
                        fechayhora:valores.fechayhora,
                        latitud:valores.latitud,
                        longitud:valores.longitud,
                        barrio:valores.barrio,
                        descripcion:valores.descripcion,
                        contacto:valores.contacto

                        }
                }),
                
             }     
        );
    };
    render(){
        let elemento=<Formik
            initialValues={
                {
                    nombre:'',
                    direccion:'',
                    fechayhora:'',
                    latitud:'',
                    longitud:'',
                    barrio:'',
                    descripcion:'',
                    contacto:''
                }

            }
            onSubmit={this.enviarForm}

            validationSchema={Yup.object().shape(
                {
                    nombre:Yup.string().typeError("Debe ser un texto").required('Campo es Obligatorio'),
                    direccion:Yup.string().required('Campo es Obligatorio'),
                    fechayhora:Yup.number().typeError("Fecha en formato Timestump").required('Campo Obligatorio'),
                    latitud:Yup.number().typeError("Debe ser un número").required("Campo Obligatorio"),
                    longitud:Yup.number().typeError("Debe ser un número").required("Campo Obligatorio"),
                    barrio:Yup.string().typeError("Debe ser un texto").required("Campo Obligatorio"),
                    descripcion:Yup.string().typeError("Debe ser un texto").required("Campo Obligatorio")
                }
            )}
            >
                <Container className="p-3">
                    
                    <Form>
                    
                        <div className="jumbotron">
                            <h1 className="text-center">Formulario Eventos</h1>
                            <p className="lead text-center"> Completar el siguiente formulario con la informacion del evento que
                                deseas agregar
                            </p>
                            <div className="form-group col-md-13">
                              <label htmlFor="id">
                                <b>Nombre</b>
                              </label>
                              <Field name="nombre" type="text" className="form-control" placeholder="Ej:Torneo de Microfutbol"/> 
                            <ErrorMessage name="nombre" className="invalid-feedback"></ErrorMessage>
                            </div>
                            <div className="row">
                            <div className="form-group col-md-7">
                            <label htmlFor="id">
                                <b>Dirección</b>
                            </label>
                            <Field name="direccion" type="text" className="form-control" placeholder="Ej: Calle 13 sur #25-14"/>
                            <ErrorMessage name="direccion" className="invalid-feedback"></ErrorMessage>
                            </div>
                            <div className="form-group col-md-5">
                                <label htmlFor="id">
                                <b>Fecha y Hora</b>
                                </label>
                                <Field name="fechayhora" type="text" className="form-control" placeholder="Ej: YYYYMMDDHHMMSS"/>
                                <ErrorMessage name="fechayhora" className="invalid-feedback"></ErrorMessage>
                            </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-5">
                                    <label htmlFor="id">
                                       <b>Latitud</b>
                                    </label>
                                    <Field name="latitud" type="text" className="form-control" placeholder="Ej: 4.9283726"/>
                                    <ErrorMessage name="latitud" className="invalid-feedback"></ErrorMessage>
                                </div>
                                <div className="form-group col-md-6 ">
                                     <label htmlFor="id">
                                       <b>Longitud</b>
                                     </label>
                                    <Field name="longitud" type="text" className="form-control" placeholder="-74.8463527"/>
                                    <ErrorMessage name="longitud" className="invalid-feedback"></ErrorMessage>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-5">
                                    <label htmlFor="id">
                                       <b>Barrio</b>
                                    </label>
                                    <Field name="barrio" type="text" className="form-control" placeholder="Ej: Banderas"/>
                                    <ErrorMessage name="barrio" className="invalid-feedback"></ErrorMessage>
                                </div>
                                <div className="form-group col-md-6 ">
                                    <label htmlFor="id">
                                       <b>Descripción</b>
                                    </label>
                                    <Field name="descripcion" type="text" className="form-control" placeholder="Maximo 40 palabras"/>
                                    <ErrorMessage name="descripcion" className="invalid-feedback"></ErrorMessage>
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group col-md-5">
                                    <label htmlFor="id">
                                       <b>Contacto</b>
                                    </label>
                                    <Field name="contacto" type="text" className="form-control" placeholder="Ej: 3118410635"/>
                                    <ErrorMessage name="contacto" className="invalid-feedback"></ErrorMessage>
                                </div>
                               
                            </div>
                            
                            <div className="form-group text-center">
                                <Button type="submit" className="btn btn-lg btn-primary m-4">Enviar</Button>
                                <Button type="reset" className="btn btn-lg btn-secondary">Cancelar</Button>
                            </div>
                        </div>
                    </Form> 
                    
                </Container>
            </Formik>;
        return elemento;
    };
}

export default FormularioEventos;