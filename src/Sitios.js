import React from 'react';
import "./estilo.css";
import L from 'leaflet';
import {Map, TileLayer, Marker, Polygon, Popup} from 'react-leaflet';
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as Yup from 'yup';
import {Container, Button} from 'react-bootstrap';

let poligono=[
]

class Sitios extends React.Component{
    enviarPal(valores,acciones)
    {
        console.log(valores);
        fetch(
            'http://localhost:4000/seleccionar',
             {
                 method:'POST',
                 
                 headers:{'Content-type':'application/json',
                 'cors':'Access-Control-Allow-Origin' },
                body:JSON.stringify( {

                
                    user:{
                    buscar:valores.buscar

                        }
                }),
                
             }   
               
        )
        .then(res => res.json())
        .then(res => {
            
            console.log(res);
            console.log(res[0].latitud);   
            console.log(res[0].longitud);   
            console.log(res[0].descripcional);  
            })
        }
        
        
    

    datos={
        lat:4.629932,
        lng:-74.151124,
        zoom:12,
    }
    render(){
        let posicion=[this.datos.lat,this.datos.lng]
        const configuracion={
            dots:true,
            infinite:true,
            speed:200,
            slidesToShow:1,
            slidesToScroll:1
        } 
        let elemento=<Formik 
            initialValues={
                {buscar:''}
            }
            
            onSubmit={this.enviarPal}>
            
            
            <div className="container">
                <img src="https://secretariageneral.gov.co/sites/default/files/logo-bogota.png" className="img-ts img-thumbnail rounded float-right" alt=""></img>
                <div className="jumbotron">
                    <div className="text-center">
                    <h1 className="text-danger">Sitios de interes</h1>
                    <img src="https://img.huffingtonpost.com/asset/5c8b7d78360000e01c6d3775.jpeg?ops=1200_630" className="img-tsa img-fluid" alt=""></img>
                    </div>
                    <h6 className="lead text-center">Busca un sitio de interes o registra un nuevo evento en el panel de arriba de "FORMULARIO EVENTOS"</h6>
                    <Form>
                        <div className="row">
                            <div className="form-group col-md-8">
                                <Field name="buscar" type="text" className="form-control form--control-lg" placeholder="Buscar"/> 
                            </div>
                            <div className="form-group col-md-4">
                                <Button type="submit" className="btn btn-danger btn-block btn-lg ">Buscar</Button>
                            </div>
                        </div>
                    </Form>
                </div>
                        <div className="row  p-3">
                            <div className="col-md-8" >
                            
                            <Map center={[4.629932,-74.151124]} zoom={12}>
                                <TileLayer
                                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                    url='https://{s}.tile.osm.org/{z}/{x}/{y}.png'/>
                                <Marker position={posicion} zoom={this.datos.zoom}></Marker>
                                <Polygon color="blue" positions={poligono}></Polygon>
                            </Map>
                            </div>
                        <div className="border border-dark rounded col-md-4">
                            <b className="text-center"> DESCRIPCION:</b>
                            
                        </div>
                        </div>
            </div>
        </Formik>;
        return elemento;
    };
}
export default Sitios;