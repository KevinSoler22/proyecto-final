let imagenes=['logoBogota.jpg','UD.png','logoKennedy.png','kennedy1.jpg','kennedy2.jpg'],
    cont=0;

function carrusel(galeria){
    galeria.addEventListener('click',e=>{
        let atras=galeria.querySelector('.AD'),
            adelante=galeria.querySelector('.AT'),
            img= galeria.querySelector('img'),
            tgt=e.target;
        if (tgt==atras){
            if(cont>0){
                img.src=imagenes[cont-1];
                cont --;
            }else{
                img.src=imagenes[imagenes.length -1];
                cont=imagenes.length -1;
            }
        }else if(tgt==adelante){
            if(cont<imagenes.length -1){
                img.src=imagenes[cont+1];
                cont ++;
            }else{
                img.src=imagenes[0];
                cont=0;
            }
        }
    });
}
document.addEventListener("DOMContentLoaded",()=>{
    let galeria =document.querySelector('.galeria');
    carrusel(galeria);
})